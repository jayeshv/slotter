import os
import jinja2

TEMPLATE_PATH = os.path.join(os.path.dirname(__file__), 'templates')  #root template folder
jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(TEMPLATE_PATH))
default_service_values = {'email': 'solotter@slotter.com',
                          'website': 'www.slotter.com',
                          'phone': '91-9916042210',
                          'address': 'Mangalil\nThenhipalam\nMalappuram\nKerala',
                          'description': """This is the default description. Use edit link to change this"""}
