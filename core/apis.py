import os
import json
import logging

import webapp2
from google.appengine.api import users
from google.appengine.api import search

from core.service import Service
#from core.bookmark import Bookmark
from core.user_profile import UserProfile

def get_restricted_result(result):
    result['description'] = result['description'][:102]
    result['title'] = result['title'][:32]
    result['sub_title'] = result['sub_title'][:32]
    result['address'] = result['address'][-52:]
    return result

def success_response():
    return {'success': True}

class SearchApi(webapp2.RequestHandler):

    def get(self):
        query = self.request.get('query')
        results = search.Index(name="service").search(query)
        user_profile = UserProfile.get_by_user(users.get_current_user())
        results = [self.format_result(each, user_profile) for each in results.results]
        self.response.out.write(json.dumps(results))

    def format_result(self, search_item, user_profile=None):
        result = dict([(x.name, x.value) for x in search_item.fields])
        result['id'] = search_item.doc_id
        if user_profile:
            result['bookmarked'] = user_profile.is_bookmarked(long(search_item.doc_id))
        else:
            result['bookmarked'] = False
        result = get_restricted_result(result)
        return result

class BookmarksApi(webapp2.RequestHandler):

    def get(self):
        user_profile = UserProfile.get_by_user(users.get_current_user())

        logging.debug(user_profile.bookmarks)

        bookmarks = [Service.get_by_id(each).dump(user_profile=user_profile) \
                         for each in user_profile.bookmarks]
        self.response.out.write(json.dumps(bookmarks))

class RecentServicesApi(webapp2.RequestHandler):

    def get(self):
        user_profile = UserProfile.get_by_user(users.get_current_user())
        recent_registrations = Service.get_recent_registrations()
        recent_registrations = [each.dump(user_profile=user_profile) for each in recent_registrations]
        self.response.out.write(json.dumps(recent_registrations))

class UserInfoApi(webapp2.RequestHandler):

    def get(self):
        user = users.get_current_user()
        loggedin = False
        user_dump = None
        if user:
            loggedin = True
            user_dump = {'name': user.nickname(), 'email': user.email()}
        self.response.out.write(json.dumps({'loggedin': loggedin,
                                            'user': user_dump}))

class ServiceLogoApi(webapp2.RequestHandler):

    def get(self):
        self.response.out.write(json.dumps({'src': '/static/img/siteBackground1.jpg'}))

class UserServiceApi(webapp2.RequestHandler):

    def get(self):
        user = users.get_current_user()
        user_services = Service.get_user_services(user)
        user_services = [each.dump() for each in user_services]
        self.response.out.write(json.dumps(user_services))

class BookmarkToggleApi(webapp2.RequestHandler):

    def post(self, service_id):
        user_profile = UserProfile.get_by_user(users.get_current_user())
        user_profile.bookmarks = long(service_id)
        user_profile.put()
        self.response.out.write(success_response())

class LoginUrlApi(webapp2.RequestHandler):

    def get(self):
        providers = [('gl', 'https://www.google.com/accounts/o8/id'),
                     ('yh', 'yahoo.com'),
                     ('ms', 'myspace.com'),
                     #('aol', 'aol.com'),
                     ('moid', 'myopenid.com')]
        redirect_url = self.request.get('redirect', '/')
        login_urls = [{'provider': each[0],
                       'url': users.create_login_url(dest_url=redirect_url, 
                                                     federated_identity=each[1])} for each in providers]
        self.response.out.write(json.dumps(login_urls))

app = webapp2.WSGIApplication([
        ('/api/loginurls/', LoginUrlApi),

        ('/api/user/bookmarks/', BookmarksApi),
        ('/api/user/bookmark/(\d+)/toggle/', BookmarkToggleApi),
        ('/api/user/info/', UserInfoApi),
        ('/api/user/services/', UserServiceApi),

        ('/api/service/search/', SearchApi),
        ('/api/service/logo/', ServiceLogoApi),
        ('/api/services/recent/', RecentServicesApi)], debug=True)
