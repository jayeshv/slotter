from google.appengine.api import search
from google.appengine.ext import db

_INDEX_NAME = "service"

class Service(db.Model):
    title = db.StringProperty()
    sub_title = db.StringProperty()
    description = db.TextProperty()
    created = db.DateTimeProperty(auto_now_add=True)
    address = db.PostalAddressProperty()
    phone = db.PhoneNumberProperty()
    email = db.EmailProperty()
    website = db.LinkProperty()
    map_location = db.GeoPtProperty()
    creator = db.UserProperty()

    def put(self):
        #FIXME using task queues
        super(Service, self).put()
        search_document = self.create_document()
        search.Index(name=_INDEX_NAME).add(search_document)

    def create_document(self):
        return search.Document(
            doc_id=str(self.key().id()),
            fields=[search.TextField(name='title', value=self.title),
                    search.TextField(name='sub_title', value=self.sub_title),
                    search.TextField(name='address', value=self.address),
                    search.TextField(name='description', value=self.description)])

    def dump(self, restricted=True, user_profile=None):
        if restricted:
            output = {'title': self.title[:30], 'sub_title': self.sub_title[:30],
                      'description': self.description[:100], 'id': self.key().id(),
                      'address': self.address[-50:]}
        else:
            output = {'title': self.title, 'sub_title': self.sub_title,
                      'description': self.description, 'id': self.key().id(),
                      'address': self.address}

        if user_profile:
            output['bookmarked'] = user_profile.is_bookmarked(self.key().id())
        else:
            output['bookmarked'] = False
        return output

    @staticmethod
    def get_recent_registrations(count=10):
        q = db.Query(Service)
        q.order("-created")
        q.fetch(count)
        return q.run()

    @staticmethod
    def get_user_services(user):
        q = db.Query(Service)
        q.filter("creator", user)
        q.order("-created")
        return q.run()
