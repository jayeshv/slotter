import os
import json

import webapp2
from google.appengine.api import users

from config import jinja_environment, default_service_values
from service import Service

def login_required(func):

    def wrapper(*args, **kw):
        request_handler = args[0]
        user = users.get_current_user()
        if not user:
            request_handler.redirect('/')
        else:
            func(*args, **kw)
    return wrapper

class IndexView(webapp2.RequestHandler):

    def get(self):
        template = jinja_environment.get_template('index_page.html')
        self.response.out.write(template.render())

class ServiceRegisterView(webapp2.RequestHandler):

    @login_required
    def get(self):
        template = jinja_environment.get_template('service_register.html')
        self.response.out.write(template.render())

    @login_required
    def post(self):
        service = Service()
        service.address = self.request.get('service_address', default_service_values['address'])
        service.description = self.request.get('service_description', default_service_values['description'])
        service.title = self.request.get('service_title', 'Slotter')
        service.sub_title = self.request.get('service_sub_title', 'Form small to big')
        service.creator = users.get_current_user()
        service.put()

        self.redirect('/service/profile/%s/' % service.key().id())

class ServiceProfileView(webapp2.RequestHandler):

    def get(self, service_id):
        service = Service.get_by_id(int(service_id))
        service.address = service.address.replace('\n', '<br />')
        service.description = "kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss kkkkll sds sdfffffffffffff fdsssss"
        #service.description.replace('\n', '<br />')
        # template = jinja_environment.get_template('service_profile.html')
        # self.response.out.write(template.render({'service': service}))
        template = jinja_environment.get_template('profile/profile_template_1.html')
        self.response.out.write(template.render({'service': service}))

class PublicProfileView(webapp2.RequestHandler):

    def get(self, service_id):
        service = Service.get_by_id(int(service_id))
        service.address = service.address.replace('\n', '<br />')
        service.description = service.description.replace('\n', '<br />')
        template = jinja_environment.get_template('public_profile.html')
        self.response.out.write(template.render({'service': service}))

class LogoutHandler(webapp2.RequestHandler):

    def get(self):
        self.redirect(users.create_logout_url("/"))

class TestHandler(webapp2.RequestHandler):

    def get(self):
        template = jinja_environment.get_template('profile/test.html')
        self.response.out.write(template.render())

app = webapp2.WSGIApplication([
        ('/', IndexView),
        ('/logout/', LogoutHandler),
        ('/test/', TestHandler),

        ('/service/register/', ServiceRegisterView),
        ('/service/profile/(\d+)/', ServiceProfileView)], debug=True)
        #('/service/profile/(\d+)/public/', PublicProfileView)], debug=True)
