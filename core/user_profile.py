from google.appengine.ext import db

class UserProfile(db.Model):
    user = db.UserProperty()
    _bookmarks = db.ListProperty(long)

    def get_bookmarks(self):
        return self._bookmarks

    def toggle_bookmark(self, service_id):
        if service_id in self._bookmarks:
            self._bookmarks.remove(service_id)
        else:
            self._bookmarks.append(service_id)

    bookmarks = property(get_bookmarks, toggle_bookmark)

    def is_bookmarked(self, service_id):
        return service_id in self.bookmarks

    @staticmethod
    def get_by_user(user):
        if user:
            user_profile = UserProfile.all().filter('user =', user).get()
            if not user_profile:
                user_profile = UserProfile()
                user_profile.user = user
                user_profile.put()
            return user_profile
        return None
