from google.appengine.ext import db

from core.service import Service

class Bookmark(db.Model):
    user = db.UserProperty()
    service = db.ReferenceProperty(Service)

    @staticmethod
    def get_bookmarks(user):
        return Bookmark.all().filter('user =', user)
