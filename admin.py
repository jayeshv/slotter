import webapp2
from google.appengine.api import search
from google.appengine.ext import db

from core.service import Service
from core.user_profile import UserProfile

class ClearDS(webapp2.RequestHandler):

    def get(self):
        services = Service.all()
        d = [each for each in services]
        db.delete(d)

        profile = UserProfile.all()
        d = [each for each in profile]
        db.delete(d)
        self.response.out.write("success")

class ClearSearchDocs(webapp2.RequestHandler):

    def get(self):
        def delete_all_in_index(index_name):
            doc_index = search.Index(name=index_name)
            while True:
                document_ids = [document.doc_id
                                for document in doc_index.list_documents(ids_only=True)]
                if not document_ids:
                    break
                doc_index.remove(document_ids)

        delete_all_in_index('service')
        self.response.out.write("success")

app = webapp2.WSGIApplication([
        ('/admin/clear_ds/', ClearDS),
        ('/admin/clear_search_docs/', ClearSearchDocs)], debug=True)
