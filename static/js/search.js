var slotter = slotter || {};
slotter.views = slotter.views || {};
slotter.views.search = slotter.views.search || {};

slotter.SearchRouter = Backbone.Router.extend({

    routes: {
	"bookmark": "showBookmarks",
	"recent": "showRecentRegistrations",
	"search?q=:query": "showSearchResults",
	".*": "handleDefault",
    },

    initialize: function(view) {
	this.view = view;
    },

    handleDefault: function() {
	if (this.view.user.loggedin) {
	    this.view.setHash("bookmark");
	}
	else {
	    this.view.setHash("recent");
	}
    },

    showBookmarks: function() {
	this.view.setActiveTitle("bookmark");
	this.view.showBookamrks();
    },

    showRecentRegistrations: function() {
	this.view.setActiveTitle("recent");
	this.view.showRecentRegistrations();
    },

    showSearchResults: function(query) {
	var decodedQuery = decodeURIComponent(query);
	this.view.showSearchResults(decodedQuery);
    }
});

slotter.views.search.SearchPage = Backbone.View.extend({

    template: $('#search_area').html(),

    events: {
	"click #search_button" : "onSearch",
	"keypress input[type=text]": "actionOnEnter",
    },

    initialize: function(user) {
	this.user = user;
    },

    onSearch: function() {
    	var searchQuery = $(this.el).find('#search_query').val();
    	this.setHash('search?q=' + encodeURIComponent(searchQuery));
    },

    render: function() {
	$(this.el).html(_.template(this.template, this.user))
	return this.el;
    },

    actionOnEnter: function(e) {
        if (e.keyCode == 13) {
    	    this.onSearch();
    	}
    },

    showSearchResults: function(query) {
	this.addSearchTtileTab(query);
	this.setActiveTitle("search_results");

    	$(this.el).find('#search_query').val(query);
    	$(this.el).find("#search_button").focus();
    	//api
	var resultsView = new slotter.views.search.ResultsView(this.user);
    	$.get('/api/service/search/', {'query': query}, this.showResults(resultsView), 'json');
    },

    initSearchSuggestions: function() {
    	$('#search_query').typeahead({
    	    items: 4,

    	    source: function (query, process) {
    		return ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'asd']
    	    }
    	});
    },

    showResults: function(resultsView) {
	$("#results_area").html('');
	$("#results_area").addClass('loader');
	return function(results) {
	    $("#results_area").removeClass('loader');
	    $("#results_area").html(resultsView.render(results));	    
	}
    },

    showBookamrks: function() {
    	if (this.user.loggedin) {
	    this.setActiveTitle('bookmark');
    	    //api
	    var resultsView = new slotter.views.search.ResultsView(this.user);
    	    $.get('/api/user/bookmarks/', this.showResults(resultsView), 'json');
    	}
    	else {
    	    this.setHash('recent');
    	}
    },

    setHash: function(hashValue) {
    	location.hash = hashValue;
    },

    showRecentRegistrations: function() {
	this.setActiveTitle('recent');
    	//api
	var resultsView = new slotter.views.search.ResultsView(this.user);
    	$.get('/api/services/recent/', {}, this.showResults(resultsView), 'json');
    },

    addSearchTtileTab: function(titleText) {
	$("ul.nav-tabs li#search_results a").text("Results for " + titleText.trunc(5));
	$("ul.nav-tabs li#search_results a").attr("href", "#search?q=" + encodeURIComponent(titleText));
	$("ul.nav-tabs li#search_results a").show();
    },

    setActiveTitle: function(tabId) {
	$("ul.nav-tabs li.active").removeClass("active");
	$("ul.nav-tabs li#" + tabId).addClass("active");
    }
});

slotter.views.search.ResultsView = Backbone.View.extend({

    initialize: function(user) {
	this.user = user;
    },

    clear: function() {
	$(this.el).html('');
    },

    render: function(resultItems) {
	this.clear();
    	for (var i=0;i<resultItems.length;i++)
    	{
    	    var st = new slotter.views.search.ResultItemView(resultItems[i], this.user);
    	    $(this.el).append(st.render());
	    st.loadContents();
    	}
	if (! resultItems.length) {
    	    $(this.el).append($("#no_result_info").html());
	}
    	return this.el;
    }
});

slotter.views.search.ResultItemView = Backbone.View.extend({

    template: $('#result_item').html(),
    resultItem: null,

    events: {
	"click #toggle_bookmark": "toggleBookmark",
    },

    initialize: function(resultItem, user) {
	this.user = user;
	this.resultItem = resultItem;
    },

    toggleBookmark: function() {
	//FIXME with decorator pattern
	if(this.user.loggedin) {
	    var element = $(this.el).find(".icon-bookmark");
	    $(element).addClass("loader-mini");
	    $.post('/api/user/bookmark/' + this.resultItem.id + '/toggle/', function(data) {
		$(element).removeClass("loader-mini");
		$(element).toggleClass("icon-white");
	    });
	    return true;
	}
	var loginDialogue = new slotter.views.LoginDialogueView();
	return loginDialogue.render();
    },

    render: function () {
	$(this.el).html(_.template(this.template, this.resultItem));
	return this.el;
    },

    loadContents: function() {
	var logoElement = $(this.el).find('.img-polaroid');
	$.get('/api/service/logo/', {}, function(data) {
	    logoElement.attr('src', data.src);
	}, 'json');
	//load available apps
    }
});
