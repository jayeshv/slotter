var slotter = slotter || {};
slotter.views = slotter.views || {};

String.prototype.trunc = function(n){
    return this.length>n ? this.substr(0,n-1) + '..': this;
};

slotter.getLoggedUserdDetails = function(callback) {
    $.get('/api/user/info/', {}, callback, 'json');
}

slotter.views.HeaderLoginMenuView = Backbone.View.extend({

    template: $("#login_link").html(),
    tagName: "li",

    events: {
	"click": "showLoginDialogue",
    },

    showLoginDialogue: function(e) {
	e.preventDefault();
	var loginDialogue = new slotter.views.LoginDialogueView();
	loginDialogue.render();
    },

    render: function() {
	$(this.el).html(this.template);
	return this.el;
    }
});

slotter.views.LoginDialogueView = Backbone.View.extend({

    el: $("#login_dialogue"),    

    render: function() {
	$(this.el).modal();
	$(this.el).find('#social_logins').html('');
	$(this.el).find('.modal-body').addClass("loader");
    	var redirectUrl = window.location.href;
	var el = this.el;
	$.get('/api/loginurls/', {'redirect': redirectUrl}, function(data) {
	    $(el).find('.modal-body').removeClass("loader");	    
    	    for (var i=0;i<data.length;i++)
    	    {
		$(el).find('#social_logins').append(new slotter.views.loginButton(data[i]).render());
	    }
	}, 'json');
	return this.el;
    },
});

slotter.views.loginButton = Backbone.View.extend({

    tagName: "a",
    className: "btn btn-large btn-block btn-info",

    initialize: function(provider) {
	this.provider = provider;
    },

    render: function() {
	$(this.el).attr("href", this.provider.url);
	switch(this.provider.provider) {
	case 'gl':
	    $(this.el).addClass("google");
	    $(this.el).html("Using Google");
	    break
	case 'yh':
	    $(this.el).addClass("yahoo");
	    $(this.el).html("Using Yahoo");
	    break
	case 'ms':
	    $(this.el).addClass("myspace");
	    $(this.el).html("Using Myspace");
	    break
	case 'moid':
	    $(this.el).addClass("openid");
	    $(this.el).html("Using MyOpenid");
	    break
	}
	return this.el;
    }
    
});

slotter.views.UserInfoView = Backbone.View.extend({

    tagName: "span",
    template: $("#user_info_link").html(),
    user: null,

    initialize: function(user) {
        this.user = user;
    },

    render: function() {
	var img_src = "https://www.gravatar.com/avatar/" + hex_md5(this.user.user.email) + ".jpg?s=30";
	this.user.user.img_src = img_src;
	return _.template(this.template, this.user);
    }
});

slotter.views.MyBusinessView = Backbone.View.extend({

    template: $("#my_business_link").html(),
    tagName: "li",
    className: "dropdown",
    user: null,

    events: {
	"click .dropdown-toggle": "showUserRegistrations",
	"click #register_service": "showRegisterForm",
    },

    showUserRegistrations: function() {
	var serviceContainer = $(this.el).find("#user_services")
	$.get('/api/user/services/', {}, function(data) {
	    if(data.length) {
		$(serviceContainer).html('<li class="divider"></li>');
	    }
	    else {
		$(serviceContainer).html('');
	    }

    	    for (var i=0;i<data.length;i++)
    	    {
		var serviceMenuItem = new slotter.views.serviceMenuItem(data[i]);
		$(serviceContainer).append(serviceMenuItem.render());
    	    }	    
	}, 'json');
    },

    initialize: function(user) {
        this.user = user;
	$(this.el).html(this.template);
	var loader = new slotter.views.LoadingView().render();
	$(this.el).find("#user_services").append(loader);
    },

    showRegisterForm: function(e) {
	e.preventDefault();
	var registerDialogue = new slotter.views.ServiceRegisterView();
	registerDialogue.render();
	registerDialogue.setFocus();
    },

    render: function() {	
	return this.el;
    }
});

slotter.views.serviceMenuItem = Backbone.View.extend({
    
    template: $("#service_menu_item").html(),
    tagName: "li",

    initialize: function(serviceItem) {
	this.serviceItem = serviceItem;
    },

    render: function() {
	$(this.el).html(_.template(this.template, this.serviceItem));
	return this.el;
    }
});

slotter.views.LoadingView = Backbone.View.extend({
    className: "loader",

    render: function() {
	return this.el;
    }
});

slotter.views.ServiceRegisterView = Backbone.View.extend({

    el: $("#service_register"),

    events: {
	"click #register": "register",
	"keypress input[type=text]": "actionOnEnter"
    },

    actionOnEnter: function(e) {
        if (e.keyCode == 13) {
            $(this.el).find("#register").focus();
            this.register();
	}
    },

    register: function() {
	//$(e.target).attr('disabled', 'disabled');
	//if valid
	$(this.el).find("#register").attr('disabled', 'disabled');
	$(this.el).find('form').submit();
    },

    render: function() {
	$(this.el).modal();
	return this.el;
    },

    setFocus: function() {
	$(this.el).find("#profile_title").focus();
    }
});

// slotter.bookmarks
